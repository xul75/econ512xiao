%% HOMEWORK 2
% Xiao Lin

% (Please see the html file)


%% question1
% "de" is the demand function I defined.
% 
de=@(p) [exp(-1-p(1))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
    exp(-1-p(2))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));exp(-1-p(3))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)))];
p1=[1,1,1]';
q1=de(p1)
q0=1-sum(q1)

%% question2
% I call four starting values pa, pb, pc, and pd.

pa=[0,1,2]';
pb=[0,0,0]';
pc=[0,1,2]';
pd=[3,2,1]';


foc1=@(p) [1-p(1)+p(1)*exp(-1-p(1))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3))) ;1-p(2)+p(2)*exp(-1-p(2))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
    1-p(3)+p(3)*exp(-1-p(3))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)))];

optset('broyden','initi',0);

%%
% start at pa:
tic
[x,fval,flag,it,fjacinv]=broyden(foc1,pa);
x
toc
%%
% start at pb:
tic
[x,fval,flag,it,fjacinv]=broyden(foc1,pb);
x
toc
%%
% start at pc:
tic
[x,fval,flag,it,fjacinv]=broyden(foc1,pc);
x
toc
%%
% start at pd:
tic
[x,fval,flag,it,fjacinv]=broyden(foc1,pd);
x
toc
%% question3

n=1000;
P=[pa,pb,pc,pd];


for k=1:4
tic

p=P(:,k);

d1=1;
d2=1;
d3=1;
for j=1:n



f1=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-x)+exp(-1-p(2))+exp(-1-p(3)));
f2=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-x)+exp(-1-p(3)));
f3=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-x));


    p1=p(1);
  
   for i=1:n
    if abs(feval(f1,p1))>eps 
        p11=p1-feval(f1,p1)/d1;
        d1=(feval(f1,p11)-feval(f1,p1))/(p11-p1);
        p1=p11;
    else 
        x1=p1;
        break
    end
    
   end
   
   
    p1=p(2);
      for i=1:n
    if abs(feval(f2,p1))>eps  
        p11=p1-feval(f2,p1)/d2;
        d2=(feval(f2,p11)-feval(f2,p1))/(p11-p1);
        p1=p11;
    else 
        x2=p1;
        break
    end
      end
   
   p1=p(3);
      for i=1:n
    if abs(feval(f3,p1))>eps 
        p11=p1-feval(f3,p1)/d3;
        d3=(feval(f3,p11)-feval(f3,p1))/(p11-p1);
        p1=p11;
    else 
        x3=p1;
        break
    end
      end
      
pp=[x1;x2;x3];

if norm(pp-p)<eps
    break
else p=pp;
end

end

if j==n
    disp('not converge')
end
It(k)=j;
X(:,k)=pp;
toc
end
It
X

%%
% We can see all starting point converges. However, it is slower than
% Broyden's Method.

%% question4


 N=1000;
 
 for k=1:4
     p0=P(:,k);
     tic
   for i=1:N
    p1=1./(1-de(p0));
    if sum(abs(p0-p1))<eps
        break
    else p0=p1;
    end
   end
     Y(:,k)=p1;
     It(k)=i;
     toc
 end  

 %% 
 % All starting points converge:
 Y
 It
 
 

%% question5
% Now we take a single secant step in the Gauss-Jacobi loop instead.
for k=1:4
tic
p=P(:,k);

d1=1;
d2=1;
d3=1;

for j=1:n



f1=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-x)+exp(-1-p(2))+exp(-1-p(3)));
f2=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-x)+exp(-1-p(3)));
f3=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-x));


    p1=p(1);
  
   
    if abs(feval(f1,p1))>eps 
        p11=p1-feval(f1,p1)/d1;
        d1=(feval(f1,p11)-feval(f1,p1))/(p11-p1);
        x1=p11;
    else 
        x1=p1;
    end
  
   
   
    p1=p(2);
     
    if abs(feval(f2,p1))>eps  
        p11=p1-feval(f2,p1)/d2;
        d2=(feval(f2,p11)-feval(f2,p1))/(p11-p1);
        x2=p11;
    else 
        x2=p1;
     
     end
   
   p1=p(3);
     
    if abs(feval(f2,p1))>eps  
        p11=p1-feval(f2,p1)/d2;
        d2=(feval(f2,p11)-feval(f2,p1))/(p11-p1);
        x3=p11;
    else 
        x3=p1;
     
     end
      
pp=[x1;x2;x3];

if norm(pp-p)<eps
    break
else p=pp;
end

end

if j==n
    disp('not converge')
end
toc
It(k)=j;
X(:,k)=pp;
end

%%
% All starting points converge, and it is slightly faster than the
% Guass-Jocobi method of Question 3. It is because though the iteration number is
% higher, the "subiteration" number is lower.
It 
X