function [f,g]=llh(b)
load('hw3.mat')
%  you did not put hw3.mat in this folder. 
n=length(y);
l=ones(n,1);
dl=ones(n,6);
for i=1:n
    l(i)=-exp(X(i,:)*b)+y(i)*X(i,:)*b-log(factorial(y(i)));
end
f=-sum(l);

for i=1:n
    dl(i,:)=-exp(X(i,:)*b)*X(i,:)'+y(i)*X(i,:)';
end
g=-sum(dl);
end