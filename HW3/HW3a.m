function theta=HW3a(Y1,Y2)  %X
iter=10000;
%Y1=sum(X)/length(X);
%Y2=exp(sum(log(X))/length(X));
thetaold=1;
for i=1:iter
thetanew=thetaold-(log(thetaold/Y1)+log(Y2)-psi(thetaold))/(1/thetaold-psi(1,thetaold));
    if abs(thetanew-thetaold)<0.00000001
        break
    else
        thetaold=thetanew;
    end
end

i;

theta(1)=thetanew;
theta(2)=thetanew/Y1;
end