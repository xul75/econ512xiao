%% Homework 3
%
%
%% Part A
Y=1.1:0.01:3;
theta=ones(length(Y),2);
for j=1:length(Y)
theta(j,:)=HW3a(Y(j),1);
end
    
plot(Y,theta(:,1))

%% Part B
load('hw3.mat')
n=length(y);
%%
% Question 1 
binit=[0;0;0;0;0;0];
%%
% (1) Fminunc
options = optimoptions('fminunc','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
[bhat1,MLHV1,exitflag1,output1]=fminunc(@llh,binit,options);
bhat1
output1.iterations
output1.funcCount
%%
% (2) Fminunc with derivative
options = optimoptions('fminunc','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
[bhat2,MLHV2,exitflag2,output2]=fminunc(@llh,binit,options);
bhat2
output2.iterations
output2.funcCount
%%
% (3) Nelder Mead
options = optimset('MaxIter',2000,'MaxFunEvals',2000,'TolX',0.0000001,'TolFun',0.0000001);
[bhat3,MLHV3,exitflag3,output3]=fminsearch(@llh,binit,options);
output3.iterations
output3.funcCount
%%
% (4) BHHH
betaold=binit;
iter=1000;
for i=1:iter
betanew=betaold+(sdi(betaold)'*sdi(betaold))^-1*gradi(betaold); %sgradi(betaold)
    if norm(betanew-betaold)<0.00000001
        break
    else
        betaold=betanew;
    end
end
bhat4=betanew;
bhat4
i

%%
% Question 2
%%
% initial Hessian & eigenvalues
heini=-(sdi(binit)'*sdi(binit))
eini=eig(heini)
%%
% estimated Hessian & eigenvalues
hees=-(sdi(bhat4)'*sdi(bhat4))
ees=eig(hees)
%%
% Question 3
options = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
[bnls,nlsv,exitflag4,output4]=fminunc(@NLLS,binit);
bnls
output4.iterations
output4.funcCount
%%
% Question 4
%%
% Covairance Matrix for BHHH estimator
covmle=(sdi(bhat4)'*sdi(bhat4)/n)^-1
%%
% Covairance Matrix for NLS estimator
covnls=(lsdi(bnls)'*lsdi(bnls)/n)^-1


