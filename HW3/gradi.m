function g=gradi(b)
load('hw3.mat')
n=length(y);
dl=ones(n,6);
for i=1:n
    dl(i,:)=-exp(X(i,:)*b)*X(i,:)'+y(i)*X(i,:)';
end
g=sum(dl)';
end