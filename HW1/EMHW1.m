%Q1
X=[1,1.5,3,4,5,7,9,10]
Y1=-2+0.5*X
Y2=-2+0.5*X.^2

figure(1);
plot(X,Y1,X,Y2);
xlabel('X') ;

%Q2
space=(20-(-10))/199;
X=(-10:space:20)'
sumX=sum(X)

%Q3
A=[2,4,6;1,7,5;3,12,4]
B=[-2;3;10]
C=A'*B

% ^(-1) is slow, learn to use \ from answer key
D=(A'*A)^(-1)*B

% E is a scalar, it schould be calculated differently
E=A'*B
F=A;
F(2,:)=[];
F(:,3)=[];
F

% the same, power operator is slow, use \
x=A^(-1)*B

%Q4
I=zeros(3,3);

% check answer key for use of kron()
B=[A I I I I;I A I I I;I I A I I; I I I A I; I I I I A]

%Q5
A=5*randn(5,3)+10
G=A(:);
H=ones(15,1);
i=find(G<10);
H(i)=0;
Ac=reshape(H,5,3)

%Q6
J=csvread('datahw1.csv',1,2);
prod=J(:,3);
ex=J(:,1);
RD=J(:,2);
cap=J(:,4);
n=length(prod);
one=ones(n,1);
stats=regstats(prod,[ex,RD,cap],'linear');
b=stats.beta
se=stats.tstat.se
t=stats.tstat.t
p=stats.tstat.pval

