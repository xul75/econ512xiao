function L=likeli(beta,gamma,u)
global X Y Z T N

LL=ones(N,T);
L=ones(N,1);
for i=1:N 
    for t=1:T
    LL(i,t)=slg(beta*X(i,t)+gamma*Z(i,t)+u)^Y(i,t)*(1-slg(beta*X(i,t)+gamma*Z(i,t)+u))^(1-Y(i,t))   ;
    end
    L(i)=1000*prod(LL(i,:));
end

end