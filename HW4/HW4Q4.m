function L=HW4Q4(beta,sigma,gamma,u,sigmabu,sigmau,Randvar)
global N
LL=ones(100,N);

rand=ones(100,1)*[beta,u]+Randvar*[sigma,sigmabu;sigmabu,sigmau];

for i=1:100
    LL(i,:)=likeli(rand(i,1),gamma,rand(i,2))';
end
L=-prod(mean(LL));
end