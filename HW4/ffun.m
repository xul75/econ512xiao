function M=ffun(y,mu,sigma)
n=10;
m=2*n;
mms=ones(m,1);
for i=1:m
mms(i)=nmoment(mu,sigma,i);
end

x=y(1:10);
w=y(11:20);

MM=ones(m,1);
for k=1:m
  MM(k)=w'*(x.^(k-1)); 
end
M=MM-mms;

end