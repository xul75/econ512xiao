function M=nmoment(mu,sigma,n)
k=floor(n/2);
m=ones(k,1);
for i=1:k
m(i)=nchoosek(n,2*i)*factd(2*i-1)*sigma^(2*i)*mu^(n-2*i);
end
M=sum(m);
end