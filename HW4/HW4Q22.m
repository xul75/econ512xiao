function L=HW4Q22(beta,sigma,gamma,randvar)
global N
LL=ones(100,N);
betav=beta+sigma*randvar;
for i=1:100
    LL(i,:)=likeli(betav(i),gamma,0)';
end
L=-prod(mean(LL));
end