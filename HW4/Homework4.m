load('hw4data.mat')
addpath('~/econ_512_2017/CEtools')
global X Y Z T N
X=data.X';
Y=data.Y';
Z=data.Z';
T=data.T;
N=data.N;


beta0=0.1;
sigma0=1;
gamma=0;
u=0;



%% Question 1
%%I didn't finish this question because when calculating GQ nodes the
%%Broyden method fails to converge

f=@(x) ffun(x,beta0,sigma0);

xinit=[1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20];
broyden(f,xinit);
%Failure to converge in broyden 
% this is because you misspecified the lig likelihood function and I dont'
% see you integrating anything there. 
%% Question 2
%%For Monte Carlo I fixed the random numbers, because it won't converge
%%if we draw different random numbers each loop
randvar=randn(100,1);
L2=HW4Q22(beta0,sigma0,gamma,randvar)
% this is wrong way to calculate likelihood. you compute likelihood and
% take product of those. it creates underflow problem. you need to
% calculate log likelihood. 

%% Question 3
f1=@(x)HW4Q22(x(1),x(2),x(3),randvar);

thetainit=[beta0,sigma0,gamma];
%'Display','iter'
options = optimset('MaxIter',5000,'MaxFunEvals',5000,'TolX',0.000001,'TolFun',0.000001);
[xhat1,MLHV1,exitflag1,output1]=fminsearch(f1,thetainit,options);
output1.iterations;
output1.funcCount;
xhat1
MLHV1
% same problem here. you need log likelihood.
%% Question 4
%%x=(beta,sigma,gamma,u,sigmabu,sigmau)

Randvar=mvnrnd([0;0],[1,0;0,1],100);
f2=@(x)HW4Q4(x(1),x(2),x(3),x(4),x(5),x(6),Randvar);

thetainit2=[beta0,sigma0,gamma,0,1,1];
[xhat2,MLHV2,exitflag2,output2]=fmincon(f2,thetainit2,options);
output2.iterations;
output2.funcCount;
%xhat2
%MLHV2

% same problem here. you need log likelihood.
